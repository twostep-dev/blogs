Rails.application.routes.draw do
  get 'sessions/new'
  get 'users/new'
  root'static_pages#index'
  resources :static_pages
  resources :settings
  resources:users
  resources :comments 
  resources :cards 

  get '/forgot', to:'users#forgot'
  post '/sendpass', to:'users#sendpass'



  get '/signup', to:'users#new'
  post'/signup',to:'users#create'
  get    '/login',   to: 'sessions#new'   
  post   '/login',   to: 'sessions#create'   
  delete '/logout',  to: 'sessions#destroy'
end
