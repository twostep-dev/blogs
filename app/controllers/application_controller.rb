class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    include SessionsHelper

    

    def load_forums
      @forum_new = Forum.order(created_at: :desc).limit(2)
    end

    def logged_in_user
        unless logged_in?
          store_location
          flash[:danger] = "Please log in."
          redirect_to login_url
        end
      end

      def correct_user
        @user = User.find(session[:user_id])
        redirect_to(root_url) unless current_user?(@user)
      end
    
      def admin_user
        redirect_to(root_url) unless current_user.admin?
      end
      
end
